(function ($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {
      $(document).ready(function () {
        $(".hijri-date-input").hijriDatePicker({
          locale: "ar-sa",
          format: "DD-MM-YYYY",
          hijriFormat: "iDD/iMM/iYYYY",
          dayViewHeaderFormat: "MMMM YYYY",
          hijriDayViewHeaderFormat: "iMMMM iYYYY",
          showSwitcher: false,
          allowInputToggle: false,
          showTodayButton: false,
          useCurrent: false,
          isRTL: true,
          viewMode: 'days',
          keepOpen: false,
          hijri: true,
          debug: false,
          showClear: true,
          showTodayButton: true,
          showClose: false
        });

        $('.date_hijri').on('click', function () {
          $('#edit-date-hijri').focus();
        });
        $('.date-icon').on('click', function () {
          $('#edit-date').focus();
        });

      });
    }
  };
})(jQuery, Drupal);

