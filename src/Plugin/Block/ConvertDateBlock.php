<?php

/**
 * @file
 * Contains \Drupal\convert_date_hijri\Plugin\Block\DonBlock.
 */

namespace Drupal\convert_date_hijri\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\convert_date_hijri\Form\ConvertDate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a convert date hijri.
 *
 * @Block(
 *   id = "convert_date_hijri",
 *   admin_label = @Translation("Convert date"),
 *   category = @Translation("Custom Convert date hijri")
 * )
 */
class ConvertDateBlock extends BlockBase implements ContainerFactoryPluginInterface {


  /**
   * Drupal\Core\Form\FormBuilderInterface definition.
   *
   * @var formBuilder
   */
  protected $formBuilder;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   *
   * @return DonBlock
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->formBuilder = $container->get('form_builder');
    return $instance;
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder->getForm(ConvertDate::class);
    return [
      '#theme' => 'custom_convert_date',
      'form' => $form,
    ];
  }

}

