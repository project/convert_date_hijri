<?php

namespace Drupal\convert_date_hijri\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hijri_format\HijriFormatManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ConvertDate extends FormBase
{
  /**
   * Hijri Manager.
   *
   * @var hijriFormatManager
   */
  protected $hijriFormatManager;

  /**
   * Constructs a new HijriDate object.
   *
   * @param hijriFormatManager $hijri_manager
   *   Hijri manager.
   */
  public function __construct(hijriFormatManager $hijri_manager)
  {
    $this->hijriFormatManager = $hijri_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('hijri_format.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'convert_date_hijri';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form = [];
    $current_date = date('Y-m-d', time());
    $current_date_hijri = $this->hijriFormatManager->convertToHijri(strtotime($current_date), 'd/m/Y', 0);
    $form['type_date'] = array(
      '#type' => 'radios',
      '#title' => '',
      '#default_value' => 0,
      '#options' => array(
        0 => $this
          ->t('هجري / ميلادي'),
        1 => $this
          ->t('ميلادي / هجري'),
      ),
      '#attributes' => [
        'id' => 'choices_type_date',
      ],
    );
    $form['field_group'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'field_group_date',
        ],
      ],
    ];
    $form['field_group']['date'] = array(
      '#type' => 'date',
      '#title' => '',
      '#required' => false,
      '#default_value' => $current_date,
//      '#field_prefix' => '<div class="date-hijri">',
//      '#field_suffix' => '<i class="date-icon fa fa-calendar" aria-hidden="true"></i></div>',
      '#states' => [
        'visible' => [
          ':input[id="choices_type_date"]' => ['value' => '1'],
        ],
      ],
    );
    $form['field_group']['date_hijri'] = array(
      '#type' => 'textfield',
      '#title' => '',
      '#required' => false,
      '#default_value' => $current_date_hijri,
      '#attributes' => array('class' => array('hijri-date-input')),
      '#field_prefix' => '<div class="date-hijri">',
      '#field_suffix' => '<i class="date-icon date_hijri fa fa-calendar-o" aria-hidden="true"></i></div>',
      '#states' => [
        'visible' => [
          ':input[id="choices_type_date"]' => ['value' => '0'],
        ],
      ],
    );


    $form['actions']['#type'] = 'actions';
    $form['field_group']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('تحويل'),
      '#ajax' => [
        'callback' => '::submitForm',
        'wrapper' => 'result_text',
        'progress' => [
          'type' => 'throbber',
          //'message' => $this->t('Converting'),
        ],
      ],
    );
    $form['help'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_text" id="result_text"></div>',
    ];
    $form['#attached']['library'][] = 'convert_date_hijri/styles';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState)
  {
    // validate form
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    //    $hijri_formatter = \Drupal::service('hijri_format.manager');

    $field_type_date = $form_state->getValue('type_date');
    if ($field_type_date == 1) {
      $field_date = $form_state->getValue('date');
      $date_hijri = $this->hijriFormatManager->convertToHijri(strtotime($field_date), 'l j F  Y ', 0);
    } else {
      $field_date = $form_state->getValue('date_hijri');
      $date_format = explode('/', $field_date);
      $date_h = $this->hijriToDate($date_format[1], $date_format[0], $date_format[2]);
      $date = jdtogregorian($date_h);
      $time = strtotime($date);

      $date_hijri = date('l j F  Y', $time);
    }

    $content = '<div class="result_text" id="result_text"><p>' . $date_hijri . '</p></div>';
    $form['help']['#markup'] = $content;

    return $form['help'];

  }


  /**
   * Convert hijri to normal date
   * {@inheritdoc}
   */
  function hijriToDate($m, $d, $y)
  {
    return (int)((11 * $y + 3) / 30) + 354 * $y +
      30 * $m - (int)(($m - 1) / 2) + $d + 1948440 - 385; // 386 by year
  }
}
